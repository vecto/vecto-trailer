﻿/*
* This file is part of VECTO.
*
* Copyright © 2012-2019 European Union
*
* Developed by Graz University of Technology,
*              Institute of Internal Combustion Engines and Thermodynamics,
*              Institute of Technical Informatics
*
* VECTO is licensed under the EUPL, Version 1.1 or - as soon they will be approved
* by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use VECTO except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/community/eupl/og_page/eupl
*
* Unless required by applicable law or agreed to in writing, VECTO
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* Authors:
*   Stefan Hausberger, hausberger@ivt.tugraz.at, IVT, Graz University of Technology
*   Christian Kreiner, christian.kreiner@tugraz.at, ITI, Graz University of Technology
*   Michael Krisper, michael.krisper@tugraz.at, ITI, Graz University of Technology
*   Raphael Luz, luz@ivt.tugraz.at, IVT, Graz University of Technology
*   Markus Quaritsch, markus.quaritsch@tugraz.at, IVT, Graz University of Technology
*   Martin Rexeis, rexeis@ivt.tugraz.at, IVT, Graz University of Technology
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.XPath;
using TUGraz.IVT.VectoXML.Writer;
using TUGraz.VectoCommon.Models;
using TUGraz.VectoCommon.Resources;
using TUGraz.VectoCore.Models.Simulation.Data;
using TUGraz.VectoCore.Utils;
using XmlDocumentType = TUGraz.VectoCore.Utils.XmlDocumentType;

namespace TUGraz.VectoCore.OutputData.XML.DeclarationReports.MonitoringReport
{
    public class XMLMonitoringReportTrailer : IXMLMonitoringReport
    {
        protected static readonly XNamespace _tns = XMLDefinitions.MONITORING_TRAILER_NAMESPACE_URI;
        
        protected const string MRF_OUTPUT_PREFIX = "m";
        protected const string MRF_INPUT_PREFIX = "mrf";
        protected const string MRF_DATATYPE_TRAILER = "VectoOutputDataType";
        
        protected readonly IXMLManufacturerReport _manufacturerReport;

        protected XElement _additionalFields;

        protected string _outputType;

        protected VectoRunData _modelData;

        protected enum OutputType {
            TrailerDataType
        }

        protected Dictionary<OutputType, Action> _additionalDataWriters;

        protected enum PlaceHolder {
            TRAILER_MAKE,
            REFRIGERATION_UNIT_MAKE,
			REFRIGERATION_UNIT_POWER_SOURCE,
			VECTO_LICENSE_NUMBER,
            AXLEGEAR,
            TYRE,
            MANUFACTURER,
            MANUFACTURERADDRESS,
            MAKE,
            AERODYNAMIC_COMPONENT
        }

        public XMLMonitoringReportTrailer(IXMLManufacturerReport manufacturerReport)
        {
            _manufacturerReport = manufacturerReport;

            _additionalDataWriters = new Dictionary<OutputType, Action>() 
            {
                { OutputType.TrailerDataType, WriteTrailer_Data }
            };

            _additionalFields = new XElement(_tns + XMLNames.MonitoringDataNode, new XAttribute(XMLNames.XMLNS, _tns));
        }

        public XDocument Report { get; protected set; }

        public void GenerateReport()
        {
            if (ManufacturerReportMissing) {
                return;
            }

            ValidateManufacturerReport();

            DetectOutputType();

            CreateRootNode();
            CreateChildNodes();
        }

        public virtual void Initialize(VectoRunData modelData)
        {
            _modelData = modelData;
        }

        protected OutputType GetOutputType()
        {
            Enum.TryParse<OutputType>(_outputType.Replace('-', '_'), out var outputType);

            return outputType;
        }

        protected void WriteTrailer_Data()
        {
            WriteBaseTrailerData();
            WriteNoEngineNoGearboxConventionalComponents();
            WriteAerodynamicComponent();
        }

        protected void WriteBaseTrailerData()
        { 
            _additionalFields.Add(
                new XElement(_tns + XMLNames.MonitoringLicenseNumber, GetPlaceholder(PlaceHolder.VECTO_LICENSE_NUMBER)),
                new XElement(_tns + XMLNames.MonitoringTrailer,
                    new XElement(_tns + XMLNames.MonitoringMake, GetPlaceholder(PlaceHolder.TRAILER_MAKE))
                ),
				(_modelData.VehicleData.InputDataTrailer.BodyworkCode == BodyWorkCode.Refrigerated)
                    ? GetRefrigerationUnitData()
                    : null
            );
        }

        protected XElement GetRefrigerationUnitData()
        {
            return new XElement(
                _tns + XMLNames.MonitoringRefrigerationUnit,
                new XElement(_tns + XMLNames.MonitoringMake, GetPlaceholder(PlaceHolder.REFRIGERATION_UNIT_MAKE)),
                new XElement(_tns + XMLNames.MonitoringPowerSource, GetPlaceholder(PlaceHolder.REFRIGERATION_UNIT_POWER_SOURCE)),
                new XElement(_tns + XMLNames.MonnitoringNetPower, double.NaN.ValueAsUnit(XMLNames.KiloWatt, 0))
			);
        }

        protected void WriteAerodynamicComponent()
        {
            _additionalFields.Add(
                new XElement(_tns + XMLNames.MonitoringAerodynamicComponent,
                    GetStandardFields(PlaceHolder.AERODYNAMIC_COMPONENT.ToString()))
            );
        }

        protected void WriteNoEngineNoGearboxConventionalComponents()
        {
            _additionalFields.Add(
                new XElement(_tns + XMLNames.MonitoringAxlegear, GetStandardFields(PlaceHolder.AXLEGEAR.ToString())),
                new XElement(_tns + XMLNames.MonitoringAxleWheels, GetAxleData())
            );
        }

        protected bool ManufacturerReportMissing => _manufacturerReport == null;

        protected void ValidateManufacturerReport()
        {
            var errors = new List<string>();
            
            _manufacturerReport.Report.Validate(
                XMLValidator.GetXMLSchema(XmlDocumentType.ManufacturerReport), 
                (o, e) => { errors.Add(e.Message); }, 
                true);

            if (errors.Count > 0) {
                LogManager.GetLogger(GetType().FullName).Warn(
                    "XML Validation of manufacturer record failed! errors: {0}", 
                    string.Join(Environment.NewLine, errors));
            }
        }

        protected void CreateRootNode()
        { 
            Report = new XDocument();
            
            Report.Add(
                new XElement(
                    _tns + XMLNames.MonitoringRootNode,
                    new XAttribute(XMLNames.XMLNS, _tns),
                    new XAttribute(XMLDeclarationNamespaces.Xsi + XMLNames.XSIType, _outputType),
                    new XAttribute(XMLNames.SchemaVersion, XMLDefinitions.MONITORING_TRAILER_SCHEMA_VERSION),
                    new XAttribute(XNamespace.Xmlns + XMLNames.XSI, XMLDeclarationNamespaces.Xsi.NamespaceName),
                    new XAttribute(XNamespace.Xmlns + XMLNames.DI, XMLDeclarationNamespaces.Di),
                    new XAttribute(
                        XMLDeclarationNamespaces.Xsi + XMLNames.SchemaLocation,
                        $"{XMLDefinitions.MONITORING__TRAILER_NAMESPACE} {AbstractXMLWriter.SchemaLocationBaseUrl}" +
                        $"{XMLDefinitions.GetSchemaFilename(XmlDocumentType.MonitoringReport)}"),
                    new XAttribute(XNamespace.Xmlns + MRF_OUTPUT_PREFIX, (GetOutputType() != OutputType.TrailerDataType)  
                        ? XMLDefinitions.DECLARATION_OUTPUT_NAMESPACE_URI_V09
                        : XMLDefinitions.DECLARATION_OUTPUT_TRAILER_NAMESPACE_URI)
                )
            );
        }

        protected void CreateChildNodes()
        {
            _additionalDataWriters[GetOutputType()].Invoke();
            
            Report.Root.Add(
                new XElement(_tns + XMLNames.ManufacturerRecord,
                    GetManufacturerData()
                ),
                _additionalFields
            );
        }

        protected void DetectOutputType()
        { 
            _outputType = OutputType.TrailerDataType.ToString();
        }

        protected static string GetPlaceholder(string item)
        {
            return $"##{item}##";
        }

        protected static string GetPlaceholder(PlaceHolder item)
        { 
            return GetPlaceholder(item.ToString());
        }

        protected object[] GetAxleData()
        {
            var numAxles = _modelData.VehicleData.AxleData?.Count(x => 
                (GetOutputType() == OutputType.TrailerDataType) 
                    ? (x.AxleType == AxleType.Trailer)
                    : (x.AxleType != AxleType.Trailer)
                ) 
                ?? 0;
            
            var axleData = new object[numAxles];

            for (var i = 0; i < axleData.Length; i++) {
                axleData[i] = new XElement(
                    _tns + XMLNames.MonitoringAxle,
                    new XAttribute(XMLNames.MonitoringAxleNumber, i + 1),
                    new XElement(_tns + XMLNames.MonitoringTyre, GetStandardFields($"{PlaceHolder.TYRE}_{i + 1}")));
            }

            return axleData;
        }

        protected object[] GetManufacturerData()
        {
            var mrfCopy = new XDocument(_manufacturerReport.Report);
            var elements = mrfCopy.Root.XPathSelectElements("./*");
            
            foreach (var element in elements) {
                element.Name = XName.Get(element.Name.LocalName, _tns.NamespaceName);
            }

            var dataDescendants = elements.First(x => x.Name.LocalName == XMLNames.MRFDataNode).DescendantsAndSelf();
            
            foreach (var node in dataDescendants) {
                var attrs = node.Attributes();
                
                var typeAttrs = attrs.Where(x => (x.Name.LocalName == XMLDefinitions.XSI_TYPE_LOCALNAME)
                    && !String.IsNullOrEmpty(x.Name.Namespace.ToString()));

                if (typeAttrs.Count() > 0) {
                    var type = typeAttrs.First();
                    type.Value = MRF_OUTPUT_PREFIX + ":" + type.Value;
                }
            }
           
            return elements.ToArray<object>();
        }

        protected object[] GetStandardFields(string prefix)
        {
            return new[] {
                new XElement(_tns + XMLNames.MonitoringManufacturer, 
                    GetPlaceholder($"{prefix}_{PlaceHolder.MANUFACTURER}")),
                new XElement(_tns + XMLNames.MonitoringManufacturerAddress, 
                    GetPlaceholder($"{prefix}_{PlaceHolder.MANUFACTURERADDRESS}")),
                new XElement(_tns + XMLNames.MonitoringMake, 
                    GetPlaceholder($"{prefix}_{PlaceHolder.MAKE}"))
            };
        }


    }
}
