﻿namespace TUGraz.VectoCore.Models.Simulation.DataBus {
	public interface IEngineControl
	{
		bool IgnitionOn { get; set; }
	}
}