﻿using Ninject.Modules;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration.DataProvider;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration.Interfaces;

namespace TUGraz.VectoCore.InputData.FileIO.XML.Declaration.NinjectModules
{
	public class XMLDeclarationInputDataV22InjectModule : NinjectModule
	{
		#region Overrides of NinjectModule

		public override void Load()
		{
			Bind<IXMLTyreDeclarationInputData>().To<XMLDeclarationTyreDataProviderV22>().Named(
				XMLDeclarationTyreDataProviderV22.QUALIFIED_XSD_TYPE);
		}

		#endregion
	}
}