﻿using Ninject.Modules;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration.DataProvider;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration.Interfaces;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration.Reader;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration.Reader.Impl;

namespace TUGraz.VectoCore.InputData.FileIO.XML.Declaration.NinjectModules
{
    public class XMLDeclarationTrailerV10InjectModule : NinjectModule
    {
        public override void Load()
		{
            Bind<IXMLTrailerDeclarationVehicleData>().To<XMLDeclarationTrailerDataProviderV10>().Named(
				XMLDeclarationTrailerDataProviderV10.QUALIFIED_XSD_TYPE);

            Bind<IXMLTrailerAxlesDeclarationInputData>().To<XMLDeclarationAxlesDataProviderTrailerV10>().Named(
				XMLDeclarationAxlesDataProviderTrailerV10.QUALIFIED_XSD_TYPE);

            Bind<IXMLTrailerAxleDeclarationInputData>().To<XMLDeclarationAxleDataProviderTrailerV10>().Named(
				XMLDeclarationAxleDataProviderTrailerV10.QUALIFIED_XSD_TYPE);

            Bind<IXMLTrailerAxlesReader>().To<XMLTrailerComponentReaderV10>()
				.Named(XMLTrailerComponentReaderV10.AXLES_READER_QUALIFIED_XSD_TYPE);

            Bind<IXMLTrailerAxleReader>().To<XMLTrailerComponentReaderV10>()
				.Named(XMLTrailerComponentReaderV10.AXLE_READER_QUALIFIED_XSD_TYPE);
        }

    }
}
