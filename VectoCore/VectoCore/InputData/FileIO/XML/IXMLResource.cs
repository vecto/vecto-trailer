using TUGraz.VectoCommon.InputData;

namespace TUGraz.VectoCore.InputData.FileIO.XML
{
	public interface IXMLResource
	{
		DataSource DataSource { get; }
	}
}
