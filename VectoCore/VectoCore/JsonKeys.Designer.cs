﻿/*
* This file is part of VECTO.
*
* Copyright © 2012-2019 European Union
*
* Developed by Graz University of Technology,
*              Institute of Internal Combustion Engines and Thermodynamics,
*              Institute of Technical Informatics
*
* VECTO is licensed under the EUPL, Version 1.1 or - as soon they will be approved
* by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use VECTO except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/community/eupl/og_page/eupl
*
* Unless required by applicable law or agreed to in writing, VECTO
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* Authors:
*   Stefan Hausberger, hausberger@ivt.tugraz.at, IVT, Graz University of Technology
*   Christian Kreiner, christian.kreiner@tugraz.at, ITI, Graz University of Technology
*   Michael Krisper, michael.krisper@tugraz.at, ITI, Graz University of Technology
*   Raphael Luz, luz@ivt.tugraz.at, IVT, Graz University of Technology
*   Markus Quaritsch, markus.quaritsch@tugraz.at, IVT, Graz University of Technology
*   Martin Rexeis, rexeis@ivt.tugraz.at, IVT, Graz University of Technology
*/

namespace TUGraz.VectoCore {
    using System;
    
    
    /// <summary>
    ///   Eine stark typisierte Ressourcenklasse zum Suchen von lokalisierten Zeichenfolgen usw.
    /// </summary>
    // Diese Klasse wurde von der StronglyTypedResourceBuilder automatisch generiert
    // -Klasse über ein Tool wie ResGen oder Visual Studio automatisch generiert.
    // Um einen Member hinzuzufügen oder zu entfernen, bearbeiten Sie die .ResX-Datei und führen dann ResGen
    // mit der /str-Option erneut aus, oder Sie erstellen Ihr VS-Projekt neu.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class JsonKeys {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal JsonKeys() {
        }
        
        /// <summary>
        ///   Gibt die zwischengespeicherte ResourceManager-Instanz zurück, die von dieser Klasse verwendet wird.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TUGraz.VectoCore.JsonKeys", typeof(JsonKeys).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Überschreibt die CurrentUICulture-Eigenschaft des aktuellen Threads für alle
        ///   Ressourcenzuordnungen, die diese stark typisierte Ressourcenklasse verwenden.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die VACC ähnelt.
        /// </summary>
        internal static string DriverData_AccelerationCurve {
            get {
                return ResourceManager.GetString("DriverData_AccelerationCurve", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Dec ähnelt.
        /// </summary>
        internal static string DriverData_Lookahead_Deceleration {
            get {
                return ResourceManager.GetString("DriverData_Lookahead_Deceleration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Enabled ähnelt.
        /// </summary>
        internal static string DriverData_Lookahead_Enabled {
            get {
                return ResourceManager.GetString("DriverData_Lookahead_Enabled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die MinSpeed ähnelt.
        /// </summary>
        internal static string DriverData_Lookahead_MinSpeed {
            get {
                return ResourceManager.GetString("DriverData_Lookahead_MinSpeed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die LAC ähnelt.
        /// </summary>
        internal static string DriverData_LookaheadCoasting {
            get {
                return ResourceManager.GetString("DriverData_LookaheadCoasting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die OverSpeedEcoRoll ähnelt.
        /// </summary>
        internal static string DriverData_OverspeedEcoRoll {
            get {
                return ResourceManager.GetString("DriverData_OverspeedEcoRoll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die MinSpeed ähnelt.
        /// </summary>
        internal static string DriverData_OverspeedEcoRoll_MinSpeed {
            get {
                return ResourceManager.GetString("DriverData_OverspeedEcoRoll_MinSpeed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Mode ähnelt.
        /// </summary>
        internal static string DriverData_OverspeedEcoRoll_Mode {
            get {
                return ResourceManager.GetString("DriverData_OverspeedEcoRoll_Mode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die OverSpeed ähnelt.
        /// </summary>
        internal static string DriverData_OverspeedEcoRoll_OverSpeed {
            get {
                return ResourceManager.GetString("DriverData_OverspeedEcoRoll_OverSpeed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die UnderSpeed ähnelt.
        /// </summary>
        internal static string DriverData_OverspeedEcoRoll_UnderSpeed {
            get {
                return ResourceManager.GetString("DriverData_OverspeedEcoRoll_UnderSpeed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die StartStop ähnelt.
        /// </summary>
        internal static string DriverData_StartStop {
            get {
                return ResourceManager.GetString("DriverData_StartStop", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Delay ähnelt.
        /// </summary>
        internal static string DriverData_StartStop_Delay {
            get {
                return ResourceManager.GetString("DriverData_StartStop_Delay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Enabled ähnelt.
        /// </summary>
        internal static string DriverData_StartStop_Enabled {
            get {
                return ResourceManager.GetString("DriverData_StartStop_Enabled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die MaxSpeed ähnelt.
        /// </summary>
        internal static string DriverData_StartStop_MaxSpeed {
            get {
                return ResourceManager.GetString("DriverData_StartStop_MaxSpeed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die MinTime ähnelt.
        /// </summary>
        internal static string DriverData_StartStop_MinTime {
            get {
                return ResourceManager.GetString("DriverData_StartStop_MinTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Displacement ähnelt.
        /// </summary>
        internal static string Engine_Displacement {
            get {
                return ResourceManager.GetString("Engine_Displacement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die FuelMap ähnelt.
        /// </summary>
        internal static string Engine_FuelConsumptionMap {
            get {
                return ResourceManager.GetString("Engine_FuelConsumptionMap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die FullLoadCurve ähnelt.
        /// </summary>
        internal static string Engine_FullLoadCurveFile {
            get {
                return ResourceManager.GetString("Engine_FullLoadCurveFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die IdlingSpeed ähnelt.
        /// </summary>
        internal static string Engine_IdleSpeed {
            get {
                return ResourceManager.GetString("Engine_IdleSpeed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Inertia ähnelt.
        /// </summary>
        internal static string Engine_Inertia {
            get {
                return ResourceManager.GetString("Engine_Inertia", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die ModelName ähnelt.
        /// </summary>
        internal static string Engine_ModelName {
            get {
                return ResourceManager.GetString("Engine_ModelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die WHTC-Motorway ähnelt.
        /// </summary>
        internal static string Engine_WHTC_Motorway {
            get {
                return ResourceManager.GetString("Engine_WHTC_Motorway", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die WHTC-Rural ähnelt.
        /// </summary>
        internal static string Engine_WHTC_Rural {
            get {
                return ResourceManager.GetString("Engine_WHTC_Rural", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die WHTC-Urban ähnelt.
        /// </summary>
        internal static string Engine_WHTC_Urban {
            get {
                return ResourceManager.GetString("Engine_WHTC_Urban", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die EaryShiftUp ähnelt.
        /// </summary>
        internal static string Gearbox_EarlyShiftUp {
            get {
                return ResourceManager.GetString("Gearbox_EarlyShiftUp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Efficiency ähnelt.
        /// </summary>
        internal static string Gearbox_Gear_Efficiency {
            get {
                return ResourceManager.GetString("Gearbox_Gear_Efficiency", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die FullLoadCurve ähnelt.
        /// </summary>
        internal static string Gearbox_Gear_FullLoadCurveFile {
            get {
                return ResourceManager.GetString("Gearbox_Gear_FullLoadCurveFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die LossMap ähnelt.
        /// </summary>
        internal static string Gearbox_Gear_LossMapFile {
            get {
                return ResourceManager.GetString("Gearbox_Gear_LossMapFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Ratio ähnelt.
        /// </summary>
        internal static string Gearbox_Gear_Ratio {
            get {
                return ResourceManager.GetString("Gearbox_Gear_Ratio", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die ShiftPolygon ähnelt.
        /// </summary>
        internal static string Gearbox_Gear_ShiftPolygonFile {
            get {
                return ResourceManager.GetString("Gearbox_Gear_ShiftPolygonFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die TCactive ähnelt.
        /// </summary>
        internal static string Gearbox_Gear_TCactive {
            get {
                return ResourceManager.GetString("Gearbox_Gear_TCactive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die GearboxType ähnelt.
        /// </summary>
        internal static string Gearbox_GearboxType {
            get {
                return ResourceManager.GetString("Gearbox_GearboxType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Gears ähnelt.
        /// </summary>
        internal static string Gearbox_Gears {
            get {
                return ResourceManager.GetString("Gearbox_Gears", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Inertia ähnelt.
        /// </summary>
        internal static string Gearbox_Inertia {
            get {
                return ResourceManager.GetString("Gearbox_Inertia", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die ModelName ähnelt.
        /// </summary>
        internal static string Gearbox_ModelName {
            get {
                return ResourceManager.GetString("Gearbox_ModelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die ShiftTime ähnelt.
        /// </summary>
        internal static string Gearbox_ShiftTime {
            get {
                return ResourceManager.GetString("Gearbox_ShiftTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die SkipGears ähnelt.
        /// </summary>
        internal static string Gearbox_SkipGears {
            get {
                return ResourceManager.GetString("Gearbox_SkipGears", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die StartAcc ähnelt.
        /// </summary>
        internal static string Gearbox_StartAcceleration {
            get {
                return ResourceManager.GetString("Gearbox_StartAcceleration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die StartSpeed ähnelt.
        /// </summary>
        internal static string Gearbox_StartSpeed {
            get {
                return ResourceManager.GetString("Gearbox_StartSpeed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die StartTqReserve ähnelt.
        /// </summary>
        internal static string Gearbox_StartTorqueReserve {
            get {
                return ResourceManager.GetString("Gearbox_StartTorqueReserve", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die TorqueConverter ähnelt.
        /// </summary>
        internal static string Gearbox_TorqueConverter {
            get {
                return ResourceManager.GetString("Gearbox_TorqueConverter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Inertia ähnelt.
        /// </summary>
        internal static string Gearbox_TorqueConverter_Inertia {
            get {
                return ResourceManager.GetString("Gearbox_TorqueConverter_Inertia", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die RefRPM ähnelt.
        /// </summary>
        internal static string Gearbox_TorqueConverter_ReferenceRPM {
            get {
                return ResourceManager.GetString("Gearbox_TorqueConverter_ReferenceRPM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die File ähnelt.
        /// </summary>
        internal static string Gearbox_TorqueConverter_TCMap {
            get {
                return ResourceManager.GetString("Gearbox_TorqueConverter_TCMap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die TqReserve ähnelt.
        /// </summary>
        internal static string Gearbox_TorqueReserve {
            get {
                return ResourceManager.GetString("Gearbox_TorqueReserve", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die TracInt ähnelt.
        /// </summary>
        internal static string Gearbox_TractionInterruption {
            get {
                return ResourceManager.GetString("Gearbox_TractionInterruption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Cycles ähnelt.
        /// </summary>
        internal static string Job_Cycles {
            get {
                return ResourceManager.GetString("Job_Cycles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die EngineOnlyMode ähnelt.
        /// </summary>
        internal static string Job_EngineOnlyMode {
            get {
                return ResourceManager.GetString("Job_EngineOnlyMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Body ähnelt.
        /// </summary>
        internal static string JsonBody {
            get {
                return ResourceManager.GetString("JsonBody", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Header ähnelt.
        /// </summary>
        internal static string JsonHeader {
            get {
                return ResourceManager.GetString("JsonHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die FileVersion ähnelt.
        /// </summary>
        internal static string JsonHeader_FileVersion {
            get {
                return ResourceManager.GetString("JsonHeader_FileVersion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die SavedInDeclMode ähnelt.
        /// </summary>
        internal static string SavedInDeclMode {
            get {
                return ResourceManager.GetString("SavedInDeclMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Angledrive ähnelt.
        /// </summary>
        internal static string Vehicle_Angledrive {
            get {
                return ResourceManager.GetString("Vehicle_Angledrive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Efficiency ähnelt.
        /// </summary>
        internal static string Vehicle_Angledrive_Efficiency {
            get {
                return ResourceManager.GetString("Vehicle_Angledrive_Efficiency", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die LossMap ähnelt.
        /// </summary>
        internal static string Vehicle_Angledrive_LossMapFile {
            get {
                return ResourceManager.GetString("Vehicle_Angledrive_LossMapFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Ratio ähnelt.
        /// </summary>
        internal static string Vehicle_Angledrive_Ratio {
            get {
                return ResourceManager.GetString("Vehicle_Angledrive_Ratio", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Type ähnelt.
        /// </summary>
        internal static string Vehicle_Angledrive_Type {
            get {
                return ResourceManager.GetString("Vehicle_Angledrive_Type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die AxleConfig ähnelt.
        /// </summary>
        internal static string Vehicle_AxleConfiguration {
            get {
                return ResourceManager.GetString("Vehicle_AxleConfiguration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Axles ähnelt.
        /// </summary>
        internal static string Vehicle_AxleConfiguration_Axles {
            get {
                return ResourceManager.GetString("Vehicle_AxleConfiguration_Axles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Type ähnelt.
        /// </summary>
        internal static string Vehicle_AxleConfiguration_Type {
            get {
                return ResourceManager.GetString("Vehicle_AxleConfiguration_Type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Inertia ähnelt.
        /// </summary>
        internal static string Vehicle_Axles_Inertia {
            get {
                return ResourceManager.GetString("Vehicle_Axles_Inertia", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die RRCISO ähnelt.
        /// </summary>
        internal static string Vehicle_Axles_RollResistanceCoefficient {
            get {
                return ResourceManager.GetString("Vehicle_Axles_RollResistanceCoefficient", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die TwinTyres ähnelt.
        /// </summary>
        internal static string Vehicle_Axles_TwinTyres {
            get {
                return ResourceManager.GetString("Vehicle_Axles_TwinTyres", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die FzISO ähnelt.
        /// </summary>
        internal static string Vehicle_Axles_TyreTestLoad {
            get {
                return ResourceManager.GetString("Vehicle_Axles_TyreTestLoad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Wheels ähnelt.
        /// </summary>
        internal static string Vehicle_Axles_Wheels {
            get {
                return ResourceManager.GetString("Vehicle_Axles_Wheels", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die CurbWeight ähnelt.
        /// </summary>
        internal static string Vehicle_CurbWeight {
            get {
                return ResourceManager.GetString("Vehicle_CurbWeight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die CurbWeightExtra ähnelt.
        /// </summary>
        internal static string Vehicle_CurbWeightExtra {
            get {
                return ResourceManager.GetString("Vehicle_CurbWeightExtra", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die CdA ähnelt.
        /// </summary>
        internal static string Vehicle_DragCoefficient {
            get {
                return ResourceManager.GetString("Vehicle_DragCoefficient", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die CdA2 ähnelt.
        /// </summary>
        internal static string Vehicle_DragCoefficientRigidTruck {
            get {
                return ResourceManager.GetString("Vehicle_DragCoefficientRigidTruck", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die rdyn ähnelt.
        /// </summary>
        internal static string Vehicle_DynamicTyreRadius {
            get {
                return ResourceManager.GetString("Vehicle_DynamicTyreRadius", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die EngineFile ähnelt.
        /// </summary>
        internal static string Vehicle_EngineFile {
            get {
                return ResourceManager.GetString("Vehicle_EngineFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die GearboxFile ähnelt.
        /// </summary>
        internal static string Vehicle_GearboxFile {
            get {
                return ResourceManager.GetString("Vehicle_GearboxFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die MassMax ähnelt.
        /// </summary>
        internal static string Vehicle_GrossVehicleMassRating {
            get {
                return ResourceManager.GetString("Vehicle_GrossVehicleMassRating", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Loading ähnelt.
        /// </summary>
        internal static string Vehicle_Loading {
            get {
                return ResourceManager.GetString("Vehicle_Loading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die PTO ähnelt.
        /// </summary>
        internal static string Vehicle_PTO {
            get {
                return ResourceManager.GetString("Vehicle_PTO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Cycle ähnelt.
        /// </summary>
        internal static string Vehicle_PTO_Cycle {
            get {
                return ResourceManager.GetString("Vehicle_PTO_Cycle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die LossMap ähnelt.
        /// </summary>
        internal static string Vehicle_PTO_LossMapFile {
            get {
                return ResourceManager.GetString("Vehicle_PTO_LossMapFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Type ähnelt.
        /// </summary>
        internal static string Vehicle_PTO_Type {
            get {
                return ResourceManager.GetString("Vehicle_PTO_Type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Retarder ähnelt.
        /// </summary>
        internal static string Vehicle_Retarder {
            get {
                return ResourceManager.GetString("Vehicle_Retarder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die File ähnelt.
        /// </summary>
        internal static string Vehicle_Retarder_LossMapFile {
            get {
                return ResourceManager.GetString("Vehicle_Retarder_LossMapFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Ratio ähnelt.
        /// </summary>
        internal static string Vehicle_Retarder_Ratio {
            get {
                return ResourceManager.GetString("Vehicle_Retarder_Ratio", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Type ähnelt.
        /// </summary>
        internal static string Vehicle_Retarder_Type {
            get {
                return ResourceManager.GetString("Vehicle_Retarder_Type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Rim ähnelt.
        /// </summary>
        internal static string Vehicle_Rim {
            get {
                return ResourceManager.GetString("Vehicle_Rim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die VehCat ähnelt.
        /// </summary>
        internal static string Vehicle_VehicleCategory {
            get {
                return ResourceManager.GetString("Vehicle_VehicleCategory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die VehicleFile ähnelt.
        /// </summary>
        internal static string Vehicle_VehicleFile {
            get {
                return ResourceManager.GetString("Vehicle_VehicleFile", resourceCulture);
            }
        }
    }
}
