﻿using Ninject;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using TUGraz.VectoCommon.InputData;
using TUGraz.VectoCommon.Models;
using TUGraz.VectoCore.InputData.FileIO.XML;
using TUGraz.VectoCore.Models.Simulation.Impl;
using TUGraz.VectoCore.OutputData;
using TUGraz.VectoCore.OutputData.FileIO;
using TUGraz.VectoCore.Utils;
using TUGraz.VectoHashing.Impl;
using XmlDocumentType = TUGraz.VectoCore.Utils.XmlDocumentType;

namespace TUGraz.VectoCore.Tests.XML.Reports
{
	[TestFixture]
	[NonParallelizable]
    public class MonitoringTrailerReportWriterTest
    {
		protected StandardKernel _kernel;
		
		[OneTimeSetUp]
		public void OneTimeSetup()
		{
			Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);

			_kernel = new StandardKernel(new VectoNinjectModule());
		}

        [
		TestCase("TestData/XML/XMLReaderDeclaration/Trailers/1_Axle_DA_refrigerated.xml", TestName="MonitoringReport_Trailer_1_Axle_DA_refrigerated"),
		TestCase("TestData/XML/XMLReaderDeclaration/Trailers/2_Axle_DB_drybox.xml", TestName="MonitoringReport_Trailer_2_Axle_DB_drybox"),
		TestCase("TestData/XML/XMLReaderDeclaration/Trailers/2_Axle_DC_drybox.xml", TestName="MonitoringReport_Trailer_2_Axle_DC_drybox"),
		TestCase("TestData/XML/XMLReaderDeclaration/Trailers/2_Axle_DC_new_tyre.xml", TestName="MonitoringReport_Trailer_2_Axle_DC_new_tyre"),
		TestCase("TestData/XML/XMLReaderDeclaration/Trailers/3_Axle_DA_drybox.xml", TestName="MonitoringReport_Trailer_3_Axle_DA_drybox")
		]
        public void TestMonitoringReport(string jobFile)
		{
			var fileWriter = new FileOutputWriter(jobFile);
			var sumWriter = new SummaryDataContainer(fileWriter);
			var jobContainer = new JobContainer(sumWriter);
			var inputReader = _kernel.Get<IXMLInputDataReader>();
			var dataProvider = inputReader.Create(jobFile);
			var runsFactory = new SimulatorFactory(ExecutionMode.Declaration, dataProvider, fileWriter) {
				WriteModalResults = false,
				ActualModalData = false,
				Validate = false,
			};

			jobContainer.AddRuns(runsFactory);

			jobContainer.Execute();
			jobContainer.WaitFinished();

			Assert.AreEqual(true, jobContainer.AllCompleted);

			var validator = new XMLValidator(XmlReader.Create(fileWriter.XMLMonitoringReportName));
			validator.ValidateXML(XmlDocumentType.MonitoringReport);
			
			Assert.IsNull(validator.ValidationError);

			ValidateMRFHash(fileWriter.XMLMonitoringReportName);

			var validatorMRF = new XMLValidator(XmlReader.Create(fileWriter.XMLFullReportName));
			validatorMRF.ValidateXML(XmlDocumentType.ManufacturerReport);
			
			Assert.IsNull(validatorMRF.ValidationError);
        }

		public void ValidateMRFHash(string reportPath)
		{
			var xmlDoc = new System.Xml.XmlDocument();
			xmlDoc.Load(reportPath);

			XmlNamespaceManager namespaces = new XmlNamespaceManager(xmlDoc.NameTable);
			namespaces.AddNamespace("ns", XMLDefinitions.MONITORING_TRAILER_NAMESPACE_URI);
			
			var signatureNode = xmlDoc.SelectSingleNode("/ns:VectoMonitoring/ns:ManufacturerRecord/ns:Signature", namespaces);
			var signatureDigest = new DigestData(signatureNode);

			var hash = XMLHashProvider.ComputeHash(xmlDoc, signatureDigest.Reference.Remove(0, 1), signatureDigest.CanonicalizationMethods,
				signatureDigest.DigestMethod);
			
			Assert.IsTrue(hash.InnerText.Equals(signatureDigest.DigestValue), 
				$"Manufacturer Report hash: {signatureDigest.DigestValue} differs from calculated hash: {hash.InnerText}");
		}

    }
}
