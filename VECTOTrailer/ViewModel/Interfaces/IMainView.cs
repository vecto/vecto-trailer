﻿using System.ComponentModel;

namespace VECTOTrailer.ViewModel.Interfaces {
	public interface IMainView
	{
		void Closing(object sender, CancelEventArgs e);
	}
}